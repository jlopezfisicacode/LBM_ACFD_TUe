clear all
close all
%load phase_final_50000_140.mat
%load phase_15000_200.mat 

%load contact_angle_700_30000.mat
%load contact_angle_85_5000.mat

%load capillar_85_2000.mat
 %load capillar_700_2000.mat
 
 %load center_gravity_30000.mat
 
 %load phase_flat_interface.mat
%load wednesday_phase_15000_200_bb

%load wednesday_phase_30000_160_bb

%load phase_15000_200
%load tuesday_contact_angle_525_50000
%load tuesday_contact_angle_725_50000
load tuesday_contact_angle_85_50000


% % % 
  K_1 = mat2gray(DENSITY_1);
   K_2 = mat2gray(DENSITY_100);
   K_3 = mat2gray(DENSITY_500);
  K_4 = mat2gray(DENSITY_1000);
  K_5 = mat2gray(DENSITY_3000);
  K_6 = mat2gray(DENSITY_5000);
  K_7 = mat2gray(DENSITY_10000);
 K_8 = mat2gray(DENSITY_20000);
  K_9 = mat2gray(DENSITY_30000);
  K_10 = mat2gray(DENSITY_40000);
 K_11 = mat2gray(DENSITY_50000);
% K_12 = mat2gray(DENSITY_50000);
% % 
% % 
figure(1)
subplot(2,2,1), imshow(K_1), title('0 steps'), hold on
subplot(2,2,2), imshow(K_2), title('100 steps'), hold on
subplot(2,2,3), imshow(K_3), title('500 steps'), hold on
subplot(2,2,4), imshow(K_4), title('1000 steps')
figure(2)
subplot(2,2,1), imshow(K_5), title('3000 steps'), hold on
subplot(2,2,2), imshow(K_6), title('5000 steps'), hold on
subplot(2,2,3), imshow(K_7), title('10000 steps'), hold on
subplot(2,2,4), imshow(K_8), title('20000 steps')
figure(3)
subplot(2,2,1), imshow(K_9), title('30000 steps'), hold on
subplot(2,2,2), imshow(K_10), title('40000'), hold on
subplot(2,2,3), imshow(K_11), title('50000'), hold on
% % subplot(2,2,4), imshow(K_12), title('40000'), hold on
