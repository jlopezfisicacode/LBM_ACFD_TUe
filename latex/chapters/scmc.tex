%\documentclass[twocolumn, 12]{article}
%\usepackage{tikz}
%\usepackage{tikz,pgffor}
%\usepackage{float}
%\usepackage{graphicx}
%\usepackage{caption}
%\usepackage{subcaption}
%\usepackage{subfig}
%\usepackage{mathtools}
%%\usepackage{natbib}
%\usepackage{bibentry}
%\bibliographystyle{plain}
%\providecommand{\keywords}
%%opening
%
%\title{LBM project }
%\author{Jose Luis Lopez 0979968}
%
%\begin{document}
%
%\maketitle
%
%\begin{abstract}
%
%This text focuses on the classical LBM implementation of
%SCMP proposed by Shan, Chen and Martys \cite{shan1993}, \cite{shan1994} and \cite{martys1996}.
%\\Single component multhipase (SCMP) system refers to a multhiphase system with a 'single chemical constituent', for instance say $H_2 O$ involving liquid and vapor phases. In contrast, multi-component system can consist of two or more chemical components such as oil and water.
%\\The multhipase system is studied extensively. The model was  also extended for solid interactions.
%\end{abstract}

\section{Lattice Boltzmann model basic framework}
Lattice Boltzmann models vastly simplify Boltzmann's original conceptual
view by reducing the number of possible particle spatial positions and microscopic momenta from a continuum to just a handful and similarly discretizing time into distinct steps. Particle positions are confined to the
nodes of the lattice. Variations in momenta that could have been due to a
continuum of velocity directions and magnitudes and varying particle mass
are reduced (in the simple D2Q9) to 8 directions, 3
magnitudes, and a single particle mass. The units involved are as follows:
\begin{itemize}
\item mass unit mu
\item lattice unit lu is the fundamental length scale
\item time unit ts
\end{itemize}
The velocity magnitude in the directions $\bar{e}_1$ through $\bar{e}_4$ is 1 lattice unite per time step or 1 lu $ts^{-1}$, and the velocity magnitude of $\bar{e}_5$ through $\bar{e}_8$ is $\sqrt{2}$ lu $ts^{-1}$ (this is just one scheme representation).
\begin{itemize}
\item the macroscopic fluid density is: $\rho=\sum_{a=0}^{8}f_a$
\item the macroscopic velocity $\bar{u}=\frac{1}{\rho} \sum_{a=0}^{8}f_a \bar{e}_a$
\end{itemize}
\subsection{Single Relaxation Time BGK}
In this simple model, streaming and collision (i.e., relaxation towards local equilibrium) look like this:
\[f_a(\bar{x}+\bar{e}_a \Delta t, t+ \Delta t)=f_a(\bar{x},t)-\frac{f_a(\bar{x},t) -  f^{eq}_a (\bar{x} ,t)}{\tau } \]
where $f_a(\bar{x}+\bar{e}_a\Delta t,t+\Delta t) = f_a(\bar{x},t)$ is the streaming part and $(f_a(\bar{x},t) - f^{eq}_a (\bar{x},t))/\tau )$ is the collision term. \\Collision of the fluid particles is considered as a relaxation towards a local equilibrium and the D2Q9 equilibrium distribution function $f^{eq}$ is defined as:
\[f_a^{eq}(\bar{x})=w_a \rho(\bar{x})\left[1+ 3\frac{\bar{e}_a \cdot \bar{u}}{c^2}+\frac{9}{2}\frac{(\bar{e}_a \cdot \bar{u})^2 }{c^4}  - \frac{3}{2} \frac{\bar{u}^2}{c^2}\right]\]
\begin{itemize}
\item In streaming, we move the direction-specific densities $f_a$ to the nearest neighbor lattice nodes
\item Note that $f_eq $ is computed in terms of a new velocity $\bar{u}_{eq}$. This is an adjusted velocity that incorporates external forces. I will implement additional forces later on
\end{itemize}
\subsection{Boundary conditions}
\begin{figure}
     \centering
     \includegraphics[width=.6\linewidth]{plots/torus}
     \caption{Periodic boundary conditions on 2D system}
      \label{fig:torus}
    \end{figure}

    \begin{figure}
         \centering
         \includegraphics[width=.6\linewidth]{plots/back}
         \caption{Mid grid bounceback  boundary conditions on 2D system}
          \label{fig:back}
        \end{figure}
\begin{itemize}
\item The Fig. \ref{fig:torus} shows the  periodic boundary conditions in a 2D system. They  are periodic in the sense that the system becomes  closed. In the particular case of 3D  the topology looks like a torus.
\item Bounceback boundary conditions are illustrated in the Fig.\ref{fig:back}. They are created to designate a particular node as a solid obstacle. Here I use the 'mid-plane' bounceback scheme in
which the densities are temporarily stored inside the solids and re-emerge
at the next time step.
\end{itemize}
\subsection{Poiseuille flow}
The Poiseuille flow in a slit driven by a constant force may be the simplest flow system that can be simulated with bounceback boundaries in LBM. It requires periodic boundary conditions in the flow direction and only bounceback boundaries along the walls. The Fig.\ref{fig:poiseuille} presents the implementation of the poiseuille flow.
\begin{figure}
         \centering
         \includegraphics[width=1\linewidth]{plots/poiseuille}
         \caption{Poiseuille flow in a slit }
          \label{fig:poiseuille}
    \end{figure}
\subsection{Porous media model}
The Fig.\ref{fig:poiseuille2} shows a simulation of a kind of simple porous media by only using bounceback boundary conditions. It starts with a random porous distribution, the first simulation is with full periodic boundary conditions and the second was done with bounceback boundary conditions in the upper and lower boundary. The flow is driven by increasing the inlet pressure at the left boundary each time step.
\begin{figure}
         \centering
         \includegraphics[width=1\linewidth]{plots/poiseuille_2}

         \includegraphics[width=1\linewidth]{plots/poiseuille_3}
         \caption{Porous media and flow vector field (quiver plot). The first one is with fully periodic boundary conditions, the second one is in a confined channel along the flow}
          \label{fig:poiseuille2}
    \end{figure}

\section{SCMP LBM model}
\subsection{Non ideal equation of state}
{\centering \textit{'In order to simulate multiple component fluids or fluids with nonideal gas equation of state, nonlocals interactions must be incorporated' }\\Shan 1993\cite{shan1993}.

}
The principal characteristic of the SCMP Shan and Chen LBM model is the incorporation of an attractive force between fluid 'particles', the interparticle potential. For this reason, the ideal gas law does not apply, because the application of the ideal gas equation assumes that there are no interactions between the atoms or molecules.
\\The ideal gas law is written as:
\[PV=nRT\]
\begin{itemize}
\item P is the pressure [atm]
\item V is volume [L]
\item n is number of mols
\item R is the gas constant [0.0821 $ L atm mol^{-1} K^{-1}]$
\item T is temperature [K]
\end{itemize}
The van der Waals  equation of state was developed to account for behaviors observed in real gases. He incorporated two constant values associated with individual gases. It is given by:
\[P=\frac{nRT}{V-nb}-a\left(\frac{n}{V}\right)^2\]
The second term on the right accounts for attractive forces between
molecules. Note that because a, n, and V are all positive, this term results in a reduction of the pressure relative to that of a perfect gas.\\The –nb term in the denominator accounts for the non-negligible volume
of molecules. If the 'hard sphere', closest packed volume of one mol
of molecules is b, then the minimum volume that can be occupied by n
mols of molecules is nb. \\As the pressure increases, the volume of the gas V
may approach nb. This will cause the denominator to approach zero and
the pressure will rise very rapidly, effectively preventing further compression.
\subsection{P-Vm  representations}
\begin{figure}
         \centering
         \includegraphics[width=1\linewidth]{plots/vdW}
         \caption{P-V plot for $CO_2$ of the Ideal gas law and van der Waals equation of state. a=3.592$l^2$ atm $mol^2$ and b=0.04267 l $mol^{-1}$, R=0.0821 l atm mol $K^{-1}$}
          \label{fig:77}
        \end{figure}
In the Fig.\ref{fig:77} the perfect gas law is non-linear (like y=1/x, because P is inversely proportional to Vm). The van der Waals EOS is illustrated for CO$_2$ at various temperatures with parameters a and b was used to plot the other curves. Temperatures were selected to illustrate supercritical, critical and subcritical behaviors.\\ At high temperatures (373K) $CO_2$ is supercritical and no distinct liquid and vapor phases can be discerned. As the temperature is decreased, a critical temperature is reached; below this temperature phase separation into liquid and vapor is possible.\\ The key difference in the EOS curves above and below the critical temperature is that above the critical temperature the curves decrease monotonically. Below the critical temperature the curves are no longer monotonic and this allows the coexistence of different molar volumes (different densities) of the substance at a single pressure.

\subsection{Interparticle forces}
The model was introduced by  Shan (1993). In order to simulate fluids with nonideal equation of state, nonlocal interactions among the 'particles' were incorporated \cite{shan1993}. Interactions with nearest neighbor particle densities f are sufficient to simulate the basic phenomena of multiphase interactions.
\\The model was applied in the D2Q9 model by adding  an attractive (cohesive) force F between nearest neighbors fluid 'particles' as follows:
\[\bar{F}(\bar{x},t)=-G \psi(\bar{x},t) \sum_{a=1}^{8}w_a \psi(\bar{x}+\bar{e}_1\Delta t,t)\bar{e}_a\]
where G is the interaction strength, $w_a$ is the weight used in the streaming and collision step, and $\psi$ is the interaction potential. The interaction potential presented in Fig.\ref{fig:potential} was taken from Shan (1993) as follows:
\[\psi(\rho) = \psi_0 exp(\rho_0 /\rho ) \]
where $\psi_0$ and $\rho_0$ are arbitrary constants. $G < 0$ for attraction between particles and the force is stronger when the density is higher. Thus, dense regions (liquid) experience a stronger cohesive force than vapor, which leads to surface tension phenomena. The attractive force in the model is included in the same way as gravity in the poiseuille flow.
\begin{figure}
         \centering
         \includegraphics[width=1\linewidth]{plots/interaction}
         \caption{Interaction potential function from Shan and Chen \cite{shan1994} with $\psi_0=4$ and $\rho_0=200$}
          \label{fig:potential}
        \end{figure}
\\It is important to note that we have incorporated only the molecular attraction
aspect of the van der Waals gas model described above; the repulsive
forces that dominate the van der Waals gas model when a gas is compressed
to near its 'hard sphere' volume are neglected in this simplest of
SCMP models\cite{sukop}.
\subsection{The SCMP LBM EOS}
\begin{figure}
         \centering
         \includegraphics[width=1\linewidth]{plots/scmp}
         \caption{SCMP EOS for different values of G.}
          \label{fig:scmp}
        \end{figure}
From the Shan-Chen model we can develop the equation of state for nonideal gases:
\[P=\rho RT + \frac{GRT}{2} \psi(\rho)^2\]

 The first term is the ideal gas law, which applies to the single component, single phase model. The second term is the nonideal part that accounts for the attractive force between the molecules and leads to a reduction in pressure (when G $<$ 0). When G is adequately negative that the EOS is subcritical (non-monotonic), phase separation can occur as the simulations proved. Detailed explanation of this equation is found in \cite{he}.
 \\In the model RT=1/3 obtaining:
 \[P=\frac{\rho}{3}+ \frac{G}{6}\psi(\rho)^2 \]

 The EOS is plotted for a series of G values in Fig.\ref{fig:scmp} . This EOS is qualitatively similar to the van der Waals EOS.
 \section{Numerical simulations}
 \subsection{flat interface}
 \begin{figure}
          \centering
          \includegraphics[width=1\linewidth]{plots/flat_interface_1}

          \includegraphics[width=1\linewidth]{plots/flat_interface_2}
        \caption{Time series evolution flat interface with two different densities.}
           \label{fig:flat_interface}
     \end{figure}
 \subsection{Phase separation with periodic boundary conditions initial density 200}
  \begin{figure}
            \centering
            \includegraphics[width=1\linewidth]{plots/phase_separation_200_1}
            \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with periodic boundary conditions. Initial density 200 + a random number [0,1].}
             \label{fig:phase_pb_1}
           \end{figure}
     \begin{figure}
                \centering
                \includegraphics[width=1\linewidth]{plots/phase_separation_200_2}
                \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with periodic boundary conditions. Initial density 200 + a random number [0,1].}
                 \label{fig:phase_pb_2}
               \end{figure}
    \begin{figure}
               \centering
               \includegraphics[width=1\linewidth]{plots/phase_separation_200_3}
               \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with bounceback boundary conditions. Initial density 200 + a random number [0,1].}
                \label{fig:phase_pb_3}
              \end{figure}
 \subsection{Phase separation with bounceback initial density 200}
 \begin{figure}
           \centering
           \includegraphics[width=1\linewidth]{plots/final_phase_bb_1}
           \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with bounceback boundary conditions. Initial density 200 + a random number [0,1].}
            \label{fig:phase_bb_1}
          \end{figure}
    \begin{figure}
               \centering
               \includegraphics[width=1\linewidth]{plots/final_phase_bb_2}
               \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with bounceback boundary conditions. Initial density 200 + a random number [0,1].}
                \label{fig:phase_bb_2}
              \end{figure}
   \begin{figure}
              \centering
              \includegraphics[width=1\linewidth]{plots/final_phase_bb_3}
              \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with bounceback boundary conditions. Initial density 200 + a random number [0,1].}
               \label{fig:phase_bb_3}
             \end{figure}
   \subsection{Phase separation with bounceback initial density 160}
   \begin{figure}
             \centering
             \includegraphics[width=1\linewidth]{plots/final_phase_bb_1_160}
             \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with bounceback boundary conditions. Initial density 160 + a random number [0,1].}
              \label{fig:phase_bb_1_160}
            \end{figure}
      \begin{figure}
                 \centering
                 \includegraphics[width=1\linewidth]{plots/final_phase_bb_2_160}
                 \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with bounceback boundary conditions. Initial density 160 + a random number [0,1].}
                  \label{fig:phase_bb_2_160}
                \end{figure}
     \begin{figure}
                \centering
                \includegraphics[width=1\linewidth]{plots/final_phase_bb_3_160}
                \caption{Time series of liquid-vapor phase separation dynamics in a 200x200 $lu^2$ with bounceback boundary conditions. Initial density 160 + a random number [0,1].}
                 \label{fig:phase_bb_3_160}
               \end{figure}
   \subsection{Phase separation with gravity}
   \begin{figure}
            \centering
            \includegraphics[width=1\linewidth]{plots/center_gravity_1}

            \includegraphics[width=1\linewidth]{plots/center_gravity_2}
          \caption{Time series evolution of a phase separation with gravity.}
             \label{fig:center_gravity}
       \end{figure}
 \subsection{Contact angle }
 \begin{figure}
           \centering
           \includegraphics[width=1\linewidth]{plots/final_contact_525_1}
           \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
            \label{fig:contact_525_1}
          \end{figure}

  \begin{figure}
            \centering
            \includegraphics[width=1\linewidth]{plots/final_contact_525_2}
            \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
             \label{fig:contact_525_2}
           \end{figure}

  \begin{figure}
             \centering
             \includegraphics[width=1\linewidth]{plots/final_contact_525_3}
             \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
              \label{fig:contact_525_3}
            \end{figure}
   \subsection{Contact angle }
   \begin{figure}
             \centering
             \includegraphics[width=1\linewidth]{plots/final_contact_725_1}
             \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
              \label{fig:contact_725_1}
            \end{figure}

    \begin{figure}
              \centering
              \includegraphics[width=1\linewidth]{plots/final_contact_725_2}
              \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
               \label{fig:contact_725_2}
             \end{figure}

    \begin{figure}
               \centering
               \includegraphics[width=1\linewidth]{plots/final_contact_725_3}
               \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
                \label{fig:contact_725_3}
              \end{figure}
              \newpage
    \subsection{Contact angle }
       \begin{figure}
                 \centering
                 \includegraphics[width=1\linewidth]{plots/final_contact_85_1}
                 \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
                  \label{fig:contact_85_1}
                \end{figure}

        \begin{figure}
                  \centering
                  \includegraphics[width=1\linewidth]{plots/final_contact_85_2}
                  \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
                   \label{fig:contact_85_2}
                 \end{figure}

        \begin{figure}
                   \centering
                   \includegraphics[width=1\linewidth]{plots/final_contact_85_3}
                   \caption{Time series evolution of a contact angle in a domain 200x200 $lu^2$.}
                    \label{fig:contact_85_3}
                  \end{figure}
    \newpage
 \subsection{Capillary rise}
 \begin{figure}
          \centering
          \includegraphics[width=1\linewidth]{plots/final_capilar_1}
          \caption{Capillary rise in a 1000x600 $lu^2$.}
           \label{fig:capilar_1}
         \end{figure}

 \begin{figure}
           \centering
           \includegraphics[width=1\linewidth]{plots/final_capilar_2}
           \caption{Capillary rise in a 1000x600 $lu^2$.}
            \label{fig:capilar_2}
          \end{figure}

 \begin{figure}
            \centering
            \includegraphics[width=1\linewidth]{plots/final_capilar_3}
            \caption{Capillary rise in a 1000x600 $lu^2$.}
             \label{fig:capilar_3}
           \end{figure}
%\newpage
%%bibliography%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{thebibliography}{1}
%
%   \bibitem{shan1993} Shan and Chen {\em Lattice Boltzmann model for simulating flows with multiple phases and components}  1993.
% \bibitem{shan1994} Shan  {\em Simulation of nonideal gases and liquid gas phase transitions by the lattice Boltzmann equation}  1994.
% \bibitem{martys1996} Martys and Chen  {\em Simulation of multicomponent fluids in complex three dimensional geometries by the lattice Boltzmann method}  1996.
%  \bibitem{sukop} Sukop {\em Lattice Boltzmann Modelling}  Springer 2007.
% \bibitem{he} He and Doolen {\em Thermodynamic Foundations of Kinetic Theory and Lattice Boltzmann Models for Multiphase Flows} 2001.
% \end{thebibliography}






%\end{document}
