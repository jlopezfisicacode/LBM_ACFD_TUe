clear all; close all; 
tau=1;  density=200; w1=4/9; w2=1/9; w3=1/36; c_squ=1/3; omega=1; nx=200; ny=200;
F=repmat(0,[nx ny 9]); A = (density+(rand(nx,ny))); FEQ=F;
for f=1:9; F(:,:,f) = A./9; end 
nstep = 3000; RHO_O = 200; PSI_O = 4; G = -120;

DENSITY = sum(F,3); DENSITY_1 = DENSITY;
UX=0; UY=0; U_SQU=UX.^2+UY.^2; U_C2=UX+UY; U_C4=-UX+UY; U_C6=-U_C2; U_C8=-U_C4;
  

    for ts=1:nstep
       %%       streaming step
    F(:,:,4)=F([2:nx 1],[ny 1:ny-1],4);F(:,:,3)=F(:,[ny 1:ny-1],3);
    F(:,:,2)=F([nx 1:nx-1],[ny 1:ny-1],2);F(:,:,5)=F([2:nx 1],:,5);
    F(:,:,1)=F([nx 1:nx-1],:,1);F(:,:,6)=F([2:nx 1],[2:ny 1],6);
    F(:,:,7)=F(:,[2:ny 1],7); F(:,:,8)=F([nx 1:nx-1],[2:ny 1],8); 
    %% density and velocity
    DENSITY=sum(F,3);
         
    UX=(sum(F(:,:,[1 2 8]),3)-sum(F(:,:,[4 5 6]),3))./DENSITY;
    UY=(sum(F(:,:,[2 3 4]),3)-sum(F(:,:,[6 7 8]),3))./DENSITY;
       %% force and interaction potential
         PSI_1 = PSI_O * exp(-RHO_O./DENSITY);
         PSI(2:nx+1,2:ny+1) = PSI_1;
         PSI(1,2:ny+1)      = PSI_1(nx,:); % up and bottom
         PSI(nx+2,2:ny+1)   = PSI_1(1,:);
         PSI(2:nx+1,1)      = PSI_1(:,ny); %left and right
         PSI(2:nx+1,ny+2)   = PSI_1(:,1);
         PSI(1,1)           = PSI_1(nx,ny); %corners
         PSI(nx+2,ny+2)     = PSI_1(1,1);
         PSI(nx+2,1)        = PSI_1(1,ny);
         PSI(1,ny+2)        = PSI_1(nx,1);
        
      for ff=2:ny+1 
          for f=2:nx+1
              X_F =  [-w2*PSI(f-1,ff)   w2*PSI(f+1,ff)  -w3*PSI(f-1,ff+1) -w3*PSI(f-1,ff-1)  w3*PSI(f+1,ff-1)  w3*PSI(f+1,ff+1)];
              Y_F =  [ w2*PSI(f,ff+1)  -w2*PSI(f,ff-1)   w3*PSI(f-1,ff+1) -w3*PSI(f-1,ff-1) -w3*PSI(f+1,ff-1)  w3*PSI(f+1,ff+1)];
                  FX(f-1,ff-1)= (-G*PSI_1(f-1,ff-1))*sum(X_F);
                  FY(f-1,ff-1)= (-G*PSI_1(f-1,ff-1))*sum(Y_F);
          end
       end   
    % velocities with force
    UX = UX + (FX./DENSITY);
    UY = UY + (FY./DENSITY); 
   %% FEQ equilibrium distribution function
    U_SQU=UX.^2+UY.^2; U_C2=UX+UY; U_C4=-UX+UY; U_C6=-U_C2; U_C8=-U_C4;
    % Calculate equilibrium distribution: stationary
    FEQ(:,:,9)=w1*DENSITY.*(1-U_SQU/(2*c_squ));
    % nearest-neighbours
    FEQ(:,:,1)=w2*DENSITY.*(1+UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
    FEQ(:,:,3)=w2*DENSITY.*(1+UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
    FEQ(:,:,5)=w2*DENSITY.*(1-UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
    FEQ(:,:,7)=w2*DENSITY.*(1-UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
    % next-nearest neighbours
    FEQ(:,:,2)=w3*DENSITY.*(1+U_C2/c_squ+0.5*(U_C2/c_squ).^2-U_SQU/(2*c_squ));
    FEQ(:,:,4)=w3*DENSITY.*(1+U_C4/c_squ+0.5*(U_C4/c_squ).^2-U_SQU/(2*c_squ));
    FEQ(:,:,6)=w3*DENSITY.*(1+U_C6/c_squ+0.5*(U_C6/c_squ).^2-U_SQU/(2*c_squ));
    FEQ(:,:,8)=w3*DENSITY.*(1+U_C8/c_squ+0.5*(U_C8/c_squ).^2-U_SQU/(2*c_squ));
        
        %% collision
    F=omega*FEQ+(1-omega)*F;
        
              if ts==100
                  DENSITY_100=DENSITY;
              end
              
             if ts == 200
                 DENSITY_200=DENSITY;
             end
           
              if ts== 300
                  DENSITY_300= DENSITY;
              end
              
              if ts==500
                  DENSITY_500=DENSITY;
              end
           
              
              if ts==1000
                  DENSITY_1000=DENSITY;
              end
              
               if ts == 2000
                  DENSITY_2000=DENSITY;
              end
              if ts == 3000
                  DENSITY_3000=DENSITY;
              end
              
    ts=ts+1; 
end
d1=sum(sum(DENSITY_1)); d2=sum(sum(DENSITY)); conservation_of_mass = d1-d2
K_1=mat2gray(DENSITY_1); K_2 = mat2gray(DENSITY_100); K_3=mat2gray(DENSITY_500); K_4=mat2gray(DENSITY_1000); K_5=mat2gray(DENSITY_2000);  K_6=mat2gray(DENSITY_3000);
figure(5), subplot(2,3,1), imshow(K_1); subplot(2,3,2),  imshow(K_2); subplot(2,3,3),  imshow(K_3); 
subplot(2,3,4),  imshow(K_4); subplot(2,3,5),  imshow(K_5); subplot(2,3,6),  imshow(K_6); 

              